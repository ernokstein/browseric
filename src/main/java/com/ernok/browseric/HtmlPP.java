package com.ernok.browseric;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.List;

import org.jdom2.Content;
import org.jdom2.Content.CType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class HtmlPP {
	public static JPanel parse(String file, MainFrame mf) throws JDOMException, IOException {
		Document doc = parseXmlDoc(file);
		Element root = doc.getRootElement();
		return parseDiv(root, mf);
	}

	public static Document parseXmlDoc(String xml) throws JDOMException, IOException {
		SAXBuilder builder = new SAXBuilder();
		Document document = (Document) builder.build(new StringReader(xml));
		return document;
	}

	public static Component parseNext(Element e, MainFrame mf) {
		if (e.getName().equals("div"))
			return parseDiv(e, mf);
		else if (e.getName().equals("p"))
			return parseP(e, mf);
		else if (e.getName().equals("button"))
			return parseButton(e, mf);
		else
			return parseP(e, mf);
	}

	public static GridBagConstraints parseConstraints(Element e) {
		GridBagConstraints ret = new GridBagConstraints();

		String attr;
		if ((attr = e.getAttributeValue("w")) != null) {
			ret.weightx = Float.parseFloat(attr);
		}
		if ((attr = e.getAttributeValue("h")) != null) {
			ret.weighty = Float.parseFloat(attr);
		}
		if ((attr = e.getAttributeValue("x")) != null) {
			ret.gridx = Integer.parseInt(attr);
		}
		if ((attr = e.getAttributeValue("y")) != null) {
			ret.gridy = Integer.parseInt(attr);
		}
		if ((attr = e.getAttributeValue("xspan")) != null) {
			ret.gridwidth = Integer.parseInt(attr);
		}
		if ((attr = e.getAttributeValue("yspan")) != null) {
			ret.gridheight = Integer.parseInt(attr);
		}
		if ((attr = e.getAttributeValue("fill")) != null) {
			if (attr.equals("both")) {
				ret.fill = GridBagConstraints.BOTH;
			} else if (attr.equals("horizontal")) {
				ret.fill = GridBagConstraints.HORIZONTAL;
			} else if (attr.equals("vertical")) {
				ret.fill = GridBagConstraints.VERTICAL;
			} else {
				ret.fill = GridBagConstraints.NONE;
			}
		} else {
			if (ret.weightx != 0.0 && ret.weighty != 0.0) {
				ret.fill = GridBagConstraints.BOTH;
			} else if (ret.weightx != 0.0) {
				ret.fill = GridBagConstraints.HORIZONTAL;
			} else if (ret.weighty != 0.0) {
				ret.fill = GridBagConstraints.VERTICAL;
			} else {
				ret.fill = GridBagConstraints.NONE;
			}
		}

		return ret;
	}

	public static JPanel parseDiv(Element e, MainFrame mf) {
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		for (Content c : e.getContent()) {
			if (c.getCType() == CType.Element) {
				Element elem = (Element) c;

				Component component = parseNext(elem, mf);
				GridBagConstraints constraints = parseConstraints(elem);

				panel.add(component, constraints);
			}
		}
		String bg = e.getAttributeValue("background");
		if (bg != null) {
			int rgb = Integer.parseInt(bg, 16);
			Color bgColor = new Color(rgb);
			panel.setBackground(bgColor);
		}

		return panel;
	}

	public static JLabel parseP(Element e, MainFrame mf) {
		StringBuilder text = new StringBuilder();
		for (Content c : e.getContent()) {
			if (c.getCType() == CType.Text) {
				text.append(c.getValue());
			}
		}
		return new JLabel(text.toString());
	}

	public static JButton parseButton(Element e, MainFrame mf) {
		StringBuilder text = new StringBuilder();
		for (Content c : e.getContent()) {
			if (c.getCType() == CType.Text) {
				text.append(c.getValue());
			}
		}
		JButton b = new JButton(text.toString());
		
		String link = e.getAttributeValue("link");
		if (link != null) {
			b.addActionListener((a) -> {
				mf.urlGo(link);
			});
		}
		
		return b;
	}
}
