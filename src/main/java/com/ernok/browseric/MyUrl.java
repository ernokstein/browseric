package com.ernok.browseric;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyUrl {

	public String host;
	public Integer port;
	public String path;

	public MyUrl(String urlString) {
		final Pattern p = Pattern.compile("^((\\d+\\.\\d+\\.\\d+\\.\\d+)(:\\d+)?)?(/.*)");
		Matcher m = p.matcher(urlString);

		if (!m.matches()) {
			throw new RuntimeException("Invalid URL format");
		}

		this.host = m.group(2);
		String port = m.group(3);
		if (port != null) {
			this.port = Integer.valueOf(port.substring(1));
		}
		this.path = m.group(4);
	}

	@Override
	public String toString() {
		return host + ":" + port + path;
	}

}
