package com.ernok.browseric;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import javax.swing.SwingUtilities;

/**
 * Browseric client
 *
 */
public class Browseric {
	public static void main(String[] args) throws UnknownHostException, IOException {
		SwingUtilities.invokeLater(() -> {
			MainFrame mainFrame = new MainFrame("main");
			mainFrame.pack();
			mainFrame.setSize(640, 480);
			mainFrame.setVisible(true);
		});
	}
}
