package com.ernok.browseric;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.List;

import org.jdom2.Content;
import org.jdom2.Content.CType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class Dsml {
	private MainFrame mf;
	
	public Dsml(MainFrame mf) {
		this.mf = mf;
	}
	
	public JPanel parse(String file) throws JDOMException, IOException {
		Document doc = parseXmlDoc(file);
		Element root = doc.getRootElement();
		return parseJPanel(root);
	}

	public Document parseXmlDoc(String xml) throws JDOMException, IOException {
		SAXBuilder builder = new SAXBuilder();
		Document document = (Document) builder.build(new StringReader(xml));
		return document;
	}

	public Component parseNext(Element e) {
		if (e.getName().equals("JPanel"))
			return parseJPanel(e);
		else if (e.getName().equals("JLabel"))
			return parseJLabel(e);
		else if (e.getName().equals("JButton"))
			return parseJButton(e);
		else
			return parseJLabel(e);
	}

	public Object parseConstraintsForGridBag(Element e) {
		GridBagConstraints ret = new GridBagConstraints();

		String attr;
		if ((attr = e.getAttributeValue("weightx")) != null) {
			ret.weightx = Float.parseFloat(attr);
		}
		if ((attr = e.getAttributeValue("weighty")) != null) {
			ret.weighty = Float.parseFloat(attr);
		}
		if ((attr = e.getAttributeValue("gridx")) != null) {
			ret.gridx = Integer.parseInt(attr);
		}
		if ((attr = e.getAttributeValue("gridy")) != null) {
			ret.gridy = Integer.parseInt(attr);
		}
		if ((attr = e.getAttributeValue("gridwidth")) != null) {
			ret.gridwidth = Integer.parseInt(attr);
		}
		if ((attr = e.getAttributeValue("gridheight")) != null) {
			ret.gridheight = Integer.parseInt(attr);
		}
		if ((attr = e.getAttributeValue("fill")) != null) {
			if (attr.equals("BOTH")) {
				ret.fill = GridBagConstraints.BOTH;
			} else if (attr.equals("HORIZONTAL")) {
				ret.fill = GridBagConstraints.HORIZONTAL;
			} else if (attr.equals("VERTICAL")) {
				ret.fill = GridBagConstraints.VERTICAL;
			} else if (attr.equals("NONE")) {
				ret.fill = GridBagConstraints.NONE;
			}
		}

		return ret;
	}

	public Object parseConstraintsForFlow(Element e) {
		GridBagConstraints ret = new GridBagConstraints();

		return ret;
	}

	public JPanel parseJPanel(Element e) {
		JPanel panel = new JPanel();
		
		// layout
		String layoutAttr = e.getAttributeValue("layout");
		if (layoutAttr != null) {
			if (layoutAttr.equals("GridBagLayout")) {
				panel.setLayout(new GridBagLayout());
			} else if (layoutAttr.equals("FlowLayout")) {
				panel.setLayout(new FlowLayout());
			}
		}
		
		// contents
		for (Content c : e.getContent()) {
			if (c.getCType() == CType.Element) {
				Element elem = (Element) c;

				Component component = parseNext(elem);
				
				if (panel.getLayout() != null) {
					if (panel.getLayout() instanceof GridBagLayout) {
						Object constraints = parseConstraintsForGridBag(elem);
						panel.add(component, constraints);
					} else if (panel.getLayout() instanceof FlowLayout) {
						Object constraints = parseConstraintsForFlow(elem);
						panel.add(component, constraints);
					}
				} else {
					panel.add(component);
				}
			}
		}
		
		// background
		String bg = e.getAttributeValue("background");
		if (bg != null) {
			int rgb = Integer.parseInt(bg, 16);
			Color bgColor = new Color(rgb);
			panel.setBackground(bgColor);
		}

		return panel;
	}

	public JLabel parseJLabel(Element e) {
		StringBuilder text = new StringBuilder();
		for (Content c : e.getContent()) {
			if (c.getCType() == CType.Text) {
				text.append(c.getValue());
			}
		}
		return new JLabel(text.toString());
	}

	public JButton parseJButton(Element e) {
		StringBuilder text = new StringBuilder();
		for (Content c : e.getContent()) {
			if (c.getCType() == CType.Text) {
				text.append(c.getValue());
			}
		}
		JButton b = new JButton(text.toString());
		
		String link = e.getAttributeValue("link");
		if (link != null) {
			b.addActionListener((a) -> {
				mf.urlGo(link);
			});
		}
		
		return b;
	}
}
