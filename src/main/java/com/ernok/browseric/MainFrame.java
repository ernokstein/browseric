package com.ernok.browseric;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.channels.InterruptedByTimeoutException;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.jdom2.JDOMException;

import com.ernok.ima.ImaProtocol;

/*
 * Main window with URL bar and display.
 */
public class MainFrame extends JFrame {

	enum FileType {
		HTML_PP, DSML, LEW, PLAIN,
	}

	private static final long serialVersionUID = 1L;

	private UrlBar urlBar;
	private DisplayPanel displayPanel;

	Thread requestThread = null;
	
	String defaultHost = "0.0.0.0";
	Integer defaultPort = 9494;
	
	String lastHost = null;
	Integer lastPort = null;

	private MyUrl lastUrl = null;

	public MainFrame(String name) {
		super(name);

		urlBar = new UrlBar(this);
		displayPanel = new DisplayPanel(this);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Container panel = getContentPane();
		{
			panel.setLayout(new GridBagLayout());

			GridBagConstraints c = new GridBagConstraints();

			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 0;
			panel.add(urlBar, c);

			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 1;
			c.weightx = 1.0;
			c.weighty = 1.0;
			panel.add(displayPanel, c);
		}
	}

	public void urlGo(String urlString) {
		cancelRequest();

		urlBar.setRequesting(true);

		requestThread = new Thread(() -> {

			try {
				String file = requestFile(urlString);

				FileType type;
				String extension = urlString.substring(urlString.lastIndexOf('.'));
				if (extension.equals(".html++")) {
					type = FileType.HTML_PP;
				} else if (extension.equals(".dsml")) {
					type = FileType.DSML;
				} else if (extension.equals(".lew")) {
					type = FileType.LEW;
				} else {
					type = FileType.PLAIN;
				}

				buildPage(file, type);
			} catch (RuntimeException | IOException e) {
				JOptionPane.showMessageDialog(null, String.format("Request error:\n\n%s", e.getMessage()));
			}

			endRequest();

		});

		requestThread.start();
	}

	public String requestFile(String urlString) throws IOException {
		// match url
		MyUrl url = new MyUrl(urlString);
		
		if (url.host == null) {
			if (lastUrl  != null) {
				url.host = lastUrl.host;
				url.port = lastUrl.port;
			} else {
				url.host = defaultHost;
				url.port = defaultPort;
			}
		} else {
			if (url.port == null) {
				url.port = defaultPort;
			}
		}
		
		urlBar.text.setText(url.toString());
		
		String file = ImaProtocol.requestFile(url.host, url.port, url.path);
		
		lastUrl = url;
		
		return file;
	}

	public void buildPage(String file, FileType type) {
		JPanel body;
		switch (type) {
			case HTML_PP: {
				try {
					body = HtmlPP.parse(file, this);
				} catch (JDOMException | IOException e) {
					body = new JPanel();
					body.add(new JLabel("Error parsing HTML++ file: " + e.getMessage()));
				}
				break;
			}

			case DSML: {
				Dsml dsml = new Dsml(this);
				try {
					body = dsml.parse(file);
				} catch (JDOMException | IOException e) {
					body = new JPanel();
					body.add(new JLabel("Error parsing DSML file: " + e.getMessage()));
				}
				break;
			}

			case LEW:
			case PLAIN:
			default: {
				body = new JPanel();
				GridBagConstraints c = new GridBagConstraints();
				c.fill = GridBagConstraints.BOTH;
				c.weightx = 1.0;
				c.weighty = 1.0;
				String html = "<html>" + file.replace("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br/>") + "</html>";
				JLabel l = new JLabel();
				l.setText(html);
				l.setBackground(new Color(0xFF0000));
				body.add(l,  c);
				break;
			}
		}
		Dimension size = getSize();
		displayPanel.setBody(body);
		pack();
		setSize(size);
	}

	public void cancelRequest() {
		if (requestThread != null) {
			requestThread.interrupt();
			endRequest();
		}
	}

	public void endRequest() {
		urlBar.setRequesting(false);
	}

}
