package com.ernok.browseric;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class UrlBar extends JPanel {
	private static final long serialVersionUID = 1L;
	
	JTextField text;
	JButton go;
	JButton cancel;
	
	public UrlBar(MainFrame mf) {
		text = new JTextField("0.0.0.0:9494/home.html++");
		go = new JButton("->");
		go.addActionListener(e -> { mf.urlGo(text.getText()); });
		cancel = new JButton("X");
		cancel.addActionListener(e -> { mf.cancelRequest(); });
		
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1.0;
		add(text, c);

		c.fill = GridBagConstraints.NONE;
		c.gridx = 1;
		c.gridy = 0;
		c.weightx = 0.0;
		add(go, c);

		c.fill = GridBagConstraints.NONE;
		c.gridx = 2;
		c.gridy = 0;
		c.weightx = 0.0;
		add(cancel, c);
	}

	public void setRequesting(boolean requesting) {
		if (requesting) {
			text.setEnabled(false);
		} else {
			text.setEnabled(true);
		}
	}
}
